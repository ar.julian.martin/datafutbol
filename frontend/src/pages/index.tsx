import React, { useState } from 'react'
import { RouterProvider, createBrowserRouter } from 'react-router-dom'

import Inicio from './Inicio'
import Competencias from './competencias/List'
import CompetenciaDetalle from './competencias/Detail'
import Equipos from './teams/List'
import EquipoDetalle from './teams/Detail'
import MatchDetail from './matches/Detail'
import NotFound from './NotFound'
import { Menu } from 'semantic-ui-react'
import { UserMenu } from './users/Menu'

const router = createBrowserRouter([
  { path:"/", element: <Inicio />},
  { path:"/competencias", element: <Competencias />},
  { path:"/competencias/:id/*", element: <CompetenciaDetalle />},
  { path:"/equipos", element: <Equipos />},
  { path:"/equipos/:id", element: <EquipoDetalle />},
  { path:"/partidos/:id", element: <MatchDetail />},
  { path:"*", element: <NotFound />},
])

const AppRouter: React.FC = () => {

  const [menuOpen, setMenuOpen] = useState(true);

  return <>
      <Menu borderless size='huge' stackable attached>
        <Menu.Item icon="bars" as="a" onClick={() => setMenuOpen(open => !open)} />
        <Menu.Item href="/"><img alt="logo" src="/favicon.ico" /></Menu.Item>
        <UserMenu />
      </Menu>
      {menuOpen && <Menu pointing secondary vertical floated>
        <Menu.Item name='Inicio' link href="/"/>
        <Menu.Item name='Competencias' link href="/competencias"/>
        <Menu.Item name='Equipos' link href="/equipos"/>
      </Menu>}
      <RouterProvider router={router} />
  </>
}

export default AppRouter
