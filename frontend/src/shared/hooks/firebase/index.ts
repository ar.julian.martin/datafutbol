// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider, FacebookAuthProvider, signInWithPopup, signOut, User, onAuthStateChanged } from "firebase/auth";
import { useEffect, useState } from "react";

// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyC0wKQ0tVHvmSTqGgQ0KNuSJqp-Ky0bnR8",
  authDomain: "tifa-e8db1.firebaseapp.com",
  databaseURL: "https://tifa-e8db1.firebaseio.com",
  projectId: "tifa-e8db1",
  storageBucket: "tifa-e8db1.appspot.com",
  messagingSenderId: "631661146573",
  appId: "1:631661146573:web:e5eaa6550744ea0ea15484"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

const providers = {
    google: GoogleAuthProvider,
    facebook: FacebookAuthProvider,
}

export const useFirebaseAuth = () => {

    const [user, setUser] = useState<User | null>();

    useEffect(() => onAuthStateChanged(auth, async user => {
        if(!user) localStorage.removeItem("token");
        else localStorage.setItem("token", await user.getIdToken());
        setUser(user)
    }), [])

    const handleSignIn = async (provider: "google" | "facebook") => {
        const AuthProvider = providers[provider]
        const authProvider = new AuthProvider();

        try {
            const result = await signInWithPopup(auth, authProvider);
            // El token de acceso de Google
            const credential = AuthProvider.credentialFromResult(result);
            if(!credential || !credential.idToken) throw new Error("Credentials not provided.")
            const token = credential.idToken;
            // Información del usuario
            setUser(result.user)
        } catch (error) {
            console.error("Error al autenticarse con Google:", error);
            setUser(undefined);
            throw error;
        }
    }
    
    const handleSignOut = async () => {
        await signOut(auth);
        localStorage.removeItem("token");
        setUser(undefined);
    }        

    return {
        user,
        handleSignIn,
        handleSignOut
    }
}