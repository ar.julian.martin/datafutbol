import { Equipo } from '@shared/components/Equipo'
import { useMatchesByDate } from '@shared/hooks/api/matches/useMatchesByDate'
import { DateTime } from 'luxon'
import React, { useState } from 'react'
import { Button, Container, Divider, Grid, Header, Loader } from 'semantic-ui-react'

const Inicio: React.FC = () => {

  const [date, setDate] = useState(DateTime.now().startOf("day"));

  const { data: partidos, loading, error } = useMatchesByDate(date);

  if(error) return <p>ERROR: { error.message }</p>
  if(loading || !partidos) return <Loader/>

  return <Container>
    <Header as="h2" textAlign='center'>
      <Header.Content>
        <Button icon='arrow left' onClick={() => setDate(date.minus({ days: 1 }))}/>
        { date.toLocaleString() }
        <Button icon='arrow right' onClick={() => setDate(date.plus({ days: 1 }))}/>
      </Header.Content>
    </Header>
    <Grid columns={2} stackable textAlign='center'>
      {partidos.map(p => <Grid.Row verticalAlign='middle'>
          <Grid.Column>
              <Equipo as="h3" {...p.equipoLocal} />
          </Grid.Column>
          <Grid.Column>
              <Equipo as="h3" espejado {...p.equipoVisitante} />
          </Grid.Column>
          <Divider vertical>{ p.resultado ? `${p.resultado.golesLocal} - ${p.resultado.golesVisitante}` : 'vs' }</Divider>
      </Grid.Row>)}
    </Grid>
  </Container>
}

export default Inicio
