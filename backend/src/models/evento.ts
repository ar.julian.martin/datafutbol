import mongoose from 'mongoose'
import { EventType, IEvento } from '../types/evento'

// Definir el esquema para los partidos
const eventSchema = new mongoose.Schema<IEvento>(
    {
		partido: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Partido',
			required: true,
		},
        type: {
            type: String,
            enum: Object.values(EventType),
            required: true,
        },
        team: {
            type: String,
            enum: ["local", "visitante"],
            required: true
        },
        minute: Number,
        description: String
    },
	{
		timestamps: true,
	}
)

// Crear el modelo de datos para los partidos
export const Evento = mongoose.model<IEvento>('Evento', eventSchema)
