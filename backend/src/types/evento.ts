import { Document } from "mongodb";
import { IPartido } from "./partido";

export enum EventType {
    Gol = "gol",
    Amonestacion = "amonestacion",
    Expulsion = "expulsion",
}

export interface IEvento extends Document {
    partido: IPartido["_id"];
    minute: number;
    type: EventType;
    description: string;
    team: "local" | "visitante";
}