import { IEquipo } from "@shared/types"
import { ICompetencia } from "@shared/types/competencia"
import axios from "axios"
import { createContext, useEffect, useState } from "react"
import { Loader } from "semantic-ui-react"

type CompetenciaContextType = {
    competencia: ICompetencia;
    agregarEquipo: (equipo: IEquipo) => Promise<void>;
    eliminarEquipo: (equipo: IEquipo) => Promise<void>;
    generarFixture: () => Promise<void>;
}

export const CompetenciaContext = createContext<CompetenciaContextType>({
    agregarEquipo: async (equipo) => {},
    eliminarEquipo: async (equipo) => {},
    generarFixture: async () => {},
    competencia: {
        _id: "",
        nombre: "Competencia de prueba",
        createdAt: new Date().toISOString(),
        updatedAt: new Date().toISOString(),
        equipos: [],
        partidos: [],
    },
})

type CompetenciaContextProps = {
    id: string;
    children: React.ReactNode;
}

export const CompetenciaContextProvider: React.FC<CompetenciaContextProps> = ({ id, children }) => {

    const [competencia, setCompetencia] = useState<ICompetencia>()

    useEffect(() => {
        axios(`/api/v1/competencias/${id}`).then((resp) => setCompetencia(resp.data))
    }, [id])

    if (!competencia) return <Loader active inline="centered" content="Loading" />

    const agregarEquipo = async (equipo: IEquipo) => {
        const idx = competencia.equipos.findIndex(e => e._id === equipo._id);
        if(idx !== -1) throw new Error("El equipo ya existe en la competencia");
        await axios.post(`/api/v1/competencias/${id}/equipos`, { equipoId: equipo._id })
        setCompetencia((c) => ({ ...c!, equipos: [...c!.equipos, equipo] }))
    }

    const eliminarEquipo = async (equipo: IEquipo) => {
        const idx = competencia.equipos.findIndex(e => e._id === equipo._id);
        if(idx < 0) throw new Error("El quipo no existe en la competencia");
        await axios.delete(`/api/v1/competencias/${id}/equipos/${equipo._id}`)
        const equipos = competencia?.equipos.filter(e => e._id !== equipo._id);
        setCompetencia({...competencia, equipos });
    }

    const generarFixture = async () => {
        if (competencia?.equipos.length! < 3) return
        const response = await axios.post(`/api/v1/competencias/${id}/partidos`)
        setCompetencia((c) => ({ ...c!, partidos: response.data }))
    }


    return <CompetenciaContext.Provider value={{ competencia, agregarEquipo, eliminarEquipo, generarFixture }}>
        {children}
    </CompetenciaContext.Provider>
}