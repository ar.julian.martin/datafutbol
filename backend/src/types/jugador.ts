import { Document } from "mongoose";
import { IEquipo } from "./equipo";

export interface IJugador extends Document {
  nombre: string;
  apellido: string;
  equipos: Array<IEquipo['_id']>;
}
