import escudo from "@shared/assets/escudo.png"
import { Image } from "semantic-ui-react"

export const Escudo = () => <Image size="mini" spaced src={escudo} />
