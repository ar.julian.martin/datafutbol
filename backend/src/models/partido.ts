import mongoose from 'mongoose'
import { IPartido } from '../types/partido'

// Definir el esquema para los partidos
const partidoSchema = new mongoose.Schema<IPartido>(
	{
		competencia: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Competencia',
			required: true,
		},
		equipoLocal: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Equipo',
			required: true,
		},
		equipoVisitante: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Equipo',
			required: true,
		},
		eventos: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Evento',
			},
		],
		resultado: {
			golesLocal: Number,
			golesVisitante: Number,
		},
		dia: Date,
		fecha: Number,
	},
	{
		timestamps: true,
	}
)

// Crear el modelo de datos para los partidos
export const Partido = mongoose.model<IPartido>('Partido', partidoSchema)
