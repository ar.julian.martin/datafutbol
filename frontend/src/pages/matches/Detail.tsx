import { Equipo } from "@shared/components/Equipo";
import { useMatch } from "@shared/hooks/api/matches/useMatch"
import { useParams } from "react-router-dom"
import { Button, Divider, Grid, Image, List, Loader, Segment } from "semantic-ui-react";
import escudo from "@shared/assets/escudo.png"
import { EventType } from "@shared/types/event";

const MatchDetail = () => {

    const { id } = useParams()

    const { match, addEvent, deleteEvent } = useMatch(id!);

    if(!match) return <Loader />

    const resultado = match.resultado;

    const eventosLocal = match.eventos.filter(e => e.team === "local")
    const eventosVisitante = match.eventos.filter(e => e.team === "visitante")

    return <Segment>
        <Grid columns={2} stackable textAlign='center'>
            <Divider vertical>{ resultado ? `${resultado.golesLocal} - ${resultado.golesVisitante}` : 'vs' }</Divider>
            <Grid.Row verticalAlign='middle'>
                <Grid.Column>
                    <Equipo as="h1" {...match.equipoLocal} />
                </Grid.Column>
                <Grid.Column>
                    <Equipo as="h1" espejado {...match.equipoVisitante} />
                </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='top'>
                {/* Eventos del equipo local */}
                <Grid.Column textAlign="left">
                    <List verticalAlign='middle'>
                        {eventosLocal.map(e => <List.Item>
                            <Image avatar src={escudo} />
                            <List.Content verticalAlign="middle">
                                <List.Header>Gol</List.Header>
                            </List.Content>
                            <List.Content floated="right">
                                <Button size='tiny' icon="trash"/>
                            </List.Content>
                        </List.Item>)}
                        <List.Item>
                            <Button onClick={() => addEvent({ type: EventType.Gol, team: "local" })}>Agregar gol</Button>
                        </List.Item>
                    </List>
                </Grid.Column>
                {/* Eventos del equipo visitante */}
                <Grid.Column textAlign="right">

                </Grid.Column>
            </Grid.Row>
        </Grid>
    </Segment>
}

export default MatchDetail