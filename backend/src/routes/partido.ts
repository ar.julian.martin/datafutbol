import express from 'express'
import * as partidosController from '../controllers/partido'
import * as eventsController from '../controllers/event'

const router = express.Router()

// Rutas para los partidos
router.get("/fecha/", partidosController.obtenerPartidosPorDia)
router.get("/fecha/:date", partidosController.obtenerPartidosPorDia)
router.get('/:id', partidosController.obtenerPartidoPorId)
router.put('/:id', partidosController.actualizarPartido)
router.delete('/:id', partidosController.borrarPartido)
router.post('/:id/events', eventsController.addEvent)
router.put('/:id/events/:eventId', eventsController.editEvent)
router.delete('/:id/events/:eventId', eventsController.deleteEvent)

export default router
