import { useFirebaseAuth } from "@shared/hooks/firebase";
import { useState } from "react";
import { Button, Form, Icon, Menu, Modal } from "semantic-ui-react";

export const UserMenu = () => {
    const [menuOpen, setMenuOpen] = useState(false);

    const { user, handleSignIn, handleSignOut } = useFirebaseAuth();

    const handleLogin = () => {
        setMenuOpen(false)
    }

    if (!user && menuOpen) return <Modal size={'mini'} open={menuOpen} onClose={() => setMenuOpen(true)}>
        <Modal.Header>Iniciar Sesion</Modal.Header>
        <Modal.Content>
            <Form>
                <Form.Input label="user" />
                <Form.Input label="pass" type="password" />
                <Button positive onClick={handleLogin}>Iniciar Sesion</Button>
            </Form>
        </Modal.Content>
        <Modal.Actions>
            <Button color='google plus' onClick={() => handleSignIn("google")}>
                <Icon name='google' /> Iniciar Sesion con Gmail
            </Button>
            <Button color='facebook' onClick={() => handleSignIn("facebook")}>
                <Icon name='facebook' /> Iniciar Sesion con Facebook
            </Button>
        </Modal.Actions>
    </Modal>

    if (!user) return <Menu.Item as='a' position='right' onClick={() => setMenuOpen(true)}>Iniciar Sesion</Menu.Item>
    else return <Menu.Menu position='right'>
        <Menu.Item icon="bell" as="a" href="/notifications" />
        <Menu.Item icon="user" as="a" onClick={handleSignOut} />
    </Menu.Menu>
}