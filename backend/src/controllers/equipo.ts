import { Request, Response } from 'express'
import { Equipo } from '../models/equipo'
import { IEquipo } from '../types/equipo'

export const crearEquipo = async (req: Request, res: Response) => {
	try {
		const equipo = new Equipo(req.body)
		await equipo.save()
		res.status(201).json(equipo)
	} catch (error: any) {
		res.status(500).json({ error: error.message })
	}
}

export const obtenerEquipos = async (req: Request, res: Response): Promise<void> => {
	try {
		const { nombre, limit = 10, skip = 0 } = req.query
		const query = nombre ? { nombre: { $regex: `${nombre}`, $options: 'i' } } : {}
		const equipos = await Equipo.find(query).limit(+limit).skip(+skip)
		res.status(200).json(equipos)
	} catch (error) {
		console.error(error)
		res.status(500).json({ message: 'Error interno del servidor' })
	}
}

export const obtenerEquipoPorId = async (req: Request, res: Response) => {
	try {
		const equipo = await Equipo.findById(req.params.id)
		if (!equipo) {
			return res.status(404).json({ error: 'Equipo no encontrado' })
		}
		res.status(200).json(equipo)
	} catch (error: any) {
		res.status(500).json({ error: error.message })
	}
}

export const actualizarEquipo = async (req: Request, res: Response) => {
	try {
		const equipo = await Equipo.findByIdAndUpdate(req.params.id, req.body, {
			new: true,
		})
		if (!equipo) {
			return res.status(404).json({ error: 'Equipo no encontrado' })
		}
		res.status(200).json(equipo)
	} catch (error: any) {
		res.status(500).json({ error: error.message })
	}
}

export const eliminarEquipo = async (req: Request, res: Response) => {
	try {
		const equipo = await Equipo.findByIdAndDelete(req.params.id)
		if (!equipo) {
			return res.status(404).json({ error: 'Equipo no encontrado' })
		}
		res.status(200).json(equipo)
	} catch (error: any) {
		res.status(500).json({ error: error.message })
	}
}
