export enum EventType {
    Gol = "gol",
    Amonestacion = "amonestacion",
    Expulsion = "expulsion",
}

export interface IEvent {
    _id: string;
    minute?: number;
    description?: number;
    type: EventType;
    team: "local" | "visitante";
}

export interface ICreateEventRequest extends Omit<IEvent, '_id'> {}