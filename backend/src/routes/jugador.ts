import express from 'express'
import * as jugadorController from '../controllers/jugador'

const router = express.Router()

// Rutas para los equipos
router.post('/', jugadorController.crearJugador)
router.get('/', jugadorController.obtenerJugadores)
router.get('/:id', jugadorController.obtenerJugadorPorId)
router.put('/:id', jugadorController.actualizarJugador)
router.delete('/:id', jugadorController.eliminarJugador)

export default router
