import axios from 'axios'
import { useState, useEffect } from 'react'
import { IPosicion } from '../../../types/posicion'

export const usePositionsTable = (id: string) => {
  const [tabla, setTabla] = useState<IPosicion[]>([])

  useEffect(() => {
    axios.get(`/api/v1/competencias/${id}/tabla`).then((resp) => setTabla(resp.data))
  }, [id])

  return { tabla }
}
