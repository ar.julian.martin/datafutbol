import { Document } from "mongoose";
import { IJugador } from "./jugador";

export interface IEquipo extends Document {
  nombre: string;
  jugadores: Array<IJugador["_id"]>
}
