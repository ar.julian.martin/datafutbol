import { Request, Response } from "express";
import { Evento } from "../models/evento";
import { EventType } from "../types/evento";
import { Partido } from "../models/partido";

export const addEvent = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const { minute, type, team, description } = req.body;

        const partido = await Partido.findById(id);

        if (!id || !partido)
            return res.status(400).json({ message: "El evento debe estar vinculado a un partido." });

        if (!type || !Object.values(EventType).includes(type)) 
            return res.status(400).json({ message: `El tipo de evento '${type}' no es válido.` });

        const newEvent = new Evento({
            partido: id,
            minute,
            type,
            team,
            description
        });

        // Guardar el evento en la base de datos
        const eventoGuardado = await newEvent.save();

        partido.eventos.push(eventoGuardado._id);
        await partido.save();

        // Responder con el evento guardado
        res.status(201).json(eventoGuardado);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al agregar el evento.", error });
    }
};

export const editEvent = async (req: Request, res: Response) => {
    try {
        const { eventId } = req.params;
        const { minute, type, description } = req.body;

        if (!type || !Object.values(EventType).includes(type)) {
            return res.status(400).json({ message: `El tipo de evento '${type}' no es válido.` });
        }

        // Buscar y actualizar el evento por ID
        const eventoActualizado = await Evento.findByIdAndUpdate(
            eventId,
            { minute, type, description },
            { new: true, runValidators: true } // Devolver el documento actualizado y aplicar validadores
        );

        if (!eventoActualizado) {
            return res.status(404).json({ message: "Evento no encontrado." });
        }

        res.status(200).json(eventoActualizado);
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al actualizar el evento." });
    }
};

export const deleteEvent = async (req: Request, res: Response) => {
    try {
        const { id, eventId } = req.params;
        
        const partido = await Partido.findById(id);

        if(!partido)
            return res.status(404).json({ message: "El evento no corresponde al partido." });

        const eventoEliminado = await Evento.findByIdAndDelete(eventId);
        
        if (!eventoEliminado)
            return res.status(404).json({ message: "Evento no encontrado." });

        partido.eventos = partido.eventos.filter(e => e.toString() !== eventId);
        await partido.save()

        res.status(200).json({ message: "Evento eliminado exitosamente." });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Error al eliminar el evento.", error });
    }
};