import mongoose from "mongoose";
import { IEquipo } from "../types/equipo";

// Definir el esquema para los equipos
const equipoSchema = new mongoose.Schema<IEquipo>({
  nombre: {
    type: String,
    required: true,
    unique: true,
  },
  jugadores: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Jugador',
    },
  ],
});

// Crear el modelo de datos para los equipos
export const Equipo = mongoose.model<IEquipo>("Equipo", equipoSchema);
