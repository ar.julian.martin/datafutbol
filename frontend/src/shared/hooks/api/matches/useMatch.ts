import { IPartido } from "@shared/types"
import { ICreateEventRequest, IEvent } from "@shared/types/event";
import axios from "axios";
import { useEffect, useState } from "react"

export const useMatch = (id: string) => {

    const [match, setMatch] = useState<IPartido>();

    useEffect(() => {
        axios.get(`/api/v1/partidos/${id}`).then(resp => setMatch(resp.data));
    }, [id]);

    const addEvent = async (data: ICreateEventRequest) => {
        const response = await axios.post(`/api/v1/partidos/${id}/events`, data);
        setMatch({...match!, eventos: [...match!.eventos, response.data]});
    }

    const updateEvent = async (event: IEvent) => {
        axios.put(`/api/v1/partidos/${id}/events/${event._id}`);
    }

    const deleteEvent = async (event: IEvent) => {
        axios.delete(`/api/v1/partidos/${id}/events/${event._id}`)
        setMatch({...match!, eventos: match!.eventos.filter(e => e === event)})
    }

    return {
        match,
        addEvent,
        updateEvent,
        deleteEvent
    }
}