const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const { override, addWebpackModuleRule } = require('customize-cra');

module.exports = override(
  // Añadir el plugin TsconfigPathsPlugin
  (config) => {
    const { resolve } = config;
    resolve.plugins = [
      ...(resolve.plugins || []),
      new TsconfigPathsPlugin({ configFile: './tsconfig.json' }),
    ];
    return config;
  },
);
