import React from 'react'
import { Header, Image } from 'semantic-ui-react'
import { IEquipo } from '../types/equipos'
import escudo from "@shared/assets/escudo.png"

export const Equipo: React.FC<IEquipo & { espejado?: boolean; as?: any }> = ({ nombre, espejado, as = 'h4' }) => {

  return espejado ? (
    <Header as={as}>
      {nombre} <Image spaced src={escudo} />
    </Header>
  ) : (
    <Header as={as}>
      <Image spaced src={escudo} /> {nombre}
    </Header>
  )
}
