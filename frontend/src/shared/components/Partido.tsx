import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Grid } from 'semantic-ui-react'
import { IPartido } from '../types/partido'
import { Equipo } from './Equipo'

/**
 * @deprecated use your own implementation instead
 */
export const Partido: React.FC<IPartido> = ({ _id, equipoLocal, equipoVisitante, resultado }) => {

  const navigate = useNavigate()  

  return (
    <Grid.Row key={_id} onClick={() => navigate(`/partidos/${_id}`)}>
      <Grid.Column textAlign="left">
        <Equipo {...equipoLocal!} />
      </Grid.Column>
      <Grid.Column verticalAlign="middle">{resultado ? `${resultado.golesLocal} - ${resultado.golesVisitante}` : 'vs'}</Grid.Column>
      <Grid.Column textAlign="right">
        <Equipo espejado {...equipoVisitante!} />
      </Grid.Column>
    </Grid.Row>
  )
}
