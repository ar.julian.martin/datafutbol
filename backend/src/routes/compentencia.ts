import express from 'express'
import * as competenciaController from '../controllers/competencia'

const router = express.Router()

// Rutas para las competencias
router.get('/', competenciaController.obtenerCompetencias)
router.post('/', competenciaController.crearCompetencia)
router.get('/:id', competenciaController.obtenerCompetenciaPorId)
router.post('/:id/equipos', competenciaController.agregarEquipoACompetencia)
router.delete('/:id/equipos/:equipoId', competenciaController.eliminarEquipoDeCompetencia)
router.post('/:id/partidos', competenciaController.generarPartidos)
router.get('/:id/tabla', competenciaController.obtenerTablaPosiciones)

export default router
