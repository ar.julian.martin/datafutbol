import { Request, Response } from 'express'
import { Jugador } from '../models/jugador'

export const crearJugador = async (req: Request, res: Response) => {
	try {
		const equipo = new Jugador(req.body)
		await equipo.save()
		res.status(201).json(equipo)
	} catch (error: any) {
		res.status(500).json({ error: error.message })
	}
}

export const obtenerJugadores = async (req: Request, res: Response): Promise<void> => {
	try {
		const { nombre, limit = 10, skip = 0 } = req.query
		const query = nombre ? { nombre: { $regex: `${nombre}`, $options: 'i' } } : {}
		const equipos = await Jugador.find(query).limit(+limit).skip(+skip)
		res.status(200).json(equipos)
	} catch (error) {
		console.error(error)
		res.status(500).json({ message: 'Error interno del servidor' })
	}
}

export const obtenerJugadorPorId = async (req: Request, res: Response) => {
	try {
		const equipo = await Jugador.findById(req.params.id)
		if (!equipo) {
			return res.status(404).json({ error: 'Jugador no encontrado' })
		}
		res.status(200).json(equipo)
	} catch (error: any) {
		res.status(500).json({ error: error.message })
	}
}

export const actualizarJugador = async (req: Request, res: Response) => {
	try {
		const jugador = await Jugador.findByIdAndUpdate(req.params.id, req.body, {
			new: true,
		})
		if (!jugador) {
			return res.status(404).json({ error: 'Jugador no encontrado' })
		}
		res.status(200).json(jugador)
	} catch (error: any) {
		res.status(500).json({ error: error.message })
	}
}

export const eliminarJugador = async (req: Request, res: Response) => {
	try {
		const jugador = await Jugador.findByIdAndDelete(req.params.id)
		if (!jugador) {
			return res.status(404).json({ error: 'Jugador no encontrado' })
		}
		res.status(200).json(jugador)
	} catch (error: any) {
		res.status(500).json({ error: error.message })
	}
}
