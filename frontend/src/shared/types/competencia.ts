import { IEquipo } from './equipos'
import { IPartido } from './partido'

export interface ICompetencia {
  _id: string
  nombre: string
  createdAt: string
  updatedAt: string
  equipos: IEquipo[]
  partidos: IPartido[]
}
