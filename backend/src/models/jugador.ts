import mongoose from "mongoose";
import { IJugador } from "../types/jugador";

// Definir el esquema para los equipos
const jugadorSchema = new mongoose.Schema<IJugador>({
  nombre: {
    type: String,
    required: true,
    unique: true,
  },
  apellido: {
    type: String,
    required: true,
    unique: true,
  },
});

// Crear el modelo de datos para los equipos
export const Jugador = mongoose.model<IJugador>("Jugador", jugadorSchema);
