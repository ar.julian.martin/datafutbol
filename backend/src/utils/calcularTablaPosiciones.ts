import { IEquipo } from '../types/equipo'
import { IPartido } from '../types/partido'

export interface TablaPosicionesItem {
	equipo: IEquipo
	puntos: number
	partidosJugados: number
	partidosGanados: number
	partidosEmpatados: number
	partidosPerdidos: number
	golesFavor: number
	golesContra: number
	diferenciaGoles: number
}

export function CalcularTablaPosiciones(equipos: IEquipo[], partidos: IPartido[]): TablaPosicionesItem[] {
	const tabla: TablaPosicionesItem[] = equipos.map((equipo) => ({
		equipo,
		puntos: 0,
		partidosJugados: 0,
		partidosGanados: 0,
		partidosEmpatados: 0,
		partidosPerdidos: 0,
		golesFavor: 0,
		golesContra: 0,
		diferenciaGoles: 0,
	}))

	partidos.forEach((partido) => {
		const equipoLocal = tabla.find((item) => item.equipo._id.toString() === partido.equipoLocal.toString())
		const equipoVisitante = tabla.find((item) => item.equipo._id.toString() === partido.equipoVisitante.toString())

		const hayResultado = (partido.resultado.golesLocal || partido.resultado.golesVisitante) !== undefined

		if (equipoLocal && equipoVisitante && hayResultado) {
			equipoLocal.partidosJugados++
			equipoVisitante.partidosJugados++

			equipoLocal.golesFavor += partido.resultado.golesLocal
			equipoLocal.golesContra += partido.resultado.golesVisitante
			equipoLocal.diferenciaGoles += partido.resultado.golesLocal - partido.resultado.golesVisitante

			equipoVisitante.golesFavor += partido.resultado.golesVisitante
			equipoVisitante.golesContra += partido.resultado.golesLocal
			equipoVisitante.diferenciaGoles += partido.resultado.golesVisitante - partido.resultado.golesLocal

			if (partido.resultado.golesLocal > partido.resultado.golesVisitante) {
				equipoLocal.puntos += 3
				equipoLocal.partidosGanados++
				equipoVisitante.partidosPerdidos++
			} else if (partido.resultado.golesLocal < partido.resultado.golesVisitante) {
				equipoVisitante.puntos += 3
				equipoVisitante.partidosGanados++
				equipoLocal.partidosPerdidos++
			} else {
				equipoLocal.puntos++
				equipoVisitante.puntos++
				equipoLocal.partidosEmpatados++
				equipoVisitante.partidosEmpatados++
			}
		}
	})

	tabla.sort((a, b) => {
		if (a.puntos !== b.puntos) {
			return b.puntos - a.puntos
		}

		if (a.diferenciaGoles !== b.diferenciaGoles) {
			return b.diferenciaGoles - a.diferenciaGoles
		}

		return b.golesFavor - a.golesFavor
	})

	return tabla
}
