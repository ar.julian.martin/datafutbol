import axios from 'axios'
import { useEffect, useState } from 'react'
import { ICompetencia } from '@shared/types/competencia'

export const useCompetencias = () => {
  const [competencias, setCompetencias] = useState<ICompetencia[]>([])

  useEffect(() => {
    axios.get('/api/v1/competencias').then((resp) => setCompetencias(resp.data))
  }, [])

  const createCompetencia = (nombre: string) => {
    axios.post('/api/v1/competencias', { nombre }).then((resp) => setCompetencias((c) => [...c, resp.data]))
  }

  return { competencias, createCompetencia }
}
