import React, { useContext } from 'react'
import { Button, List, Segment } from 'semantic-ui-react'
import { IEquipo } from '../types/equipos'
import { BuscarEquipo } from './BuscarEquipo'
import { CompetenciaContext } from '@shared/contexts/Competencia'
import { Escudo } from './Escudo'
import { useNavigate } from 'react-router-dom'

type ListaEquiposProps = {
  onSelect: (equipo: IEquipo) => void
}

export const ListaEquipos: React.FC<ListaEquiposProps> = () => {

  const navigate = useNavigate();
  const { competencia: { equipos, partidos }, agregarEquipo, eliminarEquipo } = useContext(CompetenciaContext);

  const handleEliminarEquipo = (equipo: IEquipo) => {
    if(partidos.length > 0) throw new Error("No se pueden eliminar equipos de una competencia empezada.");
    return eliminarEquipo(equipo);
  }

  return (
    <Segment>
      <BuscarEquipo onSelect={(e) => agregarEquipo(e)} />
      <List divided relaxed verticalAlign='middle'>
        {equipos.map((e) => (
          <List.Item key={e._id}>
            <Escudo/>
            <List.Content as={"a"} onClick={() => navigate(`/equipos/${e._id}`)}>{ e.nombre }</List.Content>
            <List.Content floated='right'>
              <Button icon="trash" onClick={() => handleEliminarEquipo(e)}/>
            </List.Content>
          </List.Item>
        ))}
      </List>
    </Segment>
  )
}
