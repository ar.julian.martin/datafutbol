import { IEquipo } from './equipos'

export interface IPosicion {
  equipo: IEquipo
  puntos: number
  partidosJugados: number
  partidosGanados: number
  partidosEmpatados: number
  partidosPerdidos: number
  golesFavor: number
  golesContra: number
  diferenciaGoles: number
}
