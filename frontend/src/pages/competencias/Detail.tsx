import React, { useContext } from 'react'
import { Route, Routes, useLocation, useNavigate, useParams } from 'react-router-dom'
import { Header, Breadcrumb, Container, Segment, Icon, Button, Menu } from 'semantic-ui-react'
import { ListaEquipos } from '@shared/components/ListaEquipos'
import { TablaPosiciones } from '@shared/components/TablaPosiciones'
import { Fixture } from '@shared/components/Fixture'
import { ChipNavigator, IChipElement } from '@shared/components/ChipsNavigator'
import { CompetenciaContext, CompetenciaContextProvider } from '@shared/contexts/Competencia'

const routes: IChipElement[] = [
  { icon: 'ordered list', text: 'Posiciones', url: './' },
  { icon: 'shield', text: 'Equipos', url: './equipos' },
  { icon: 'calendar alternate', text: 'Fixture', url: './fixture' },
  { icon: 'star', text: 'Estadisticas', url: './estadisticas' },
]

const CompetenciaDetalle: React.FC = () => {
  const { id } = useParams()
  const navigate = useNavigate()

  return (
    <CompetenciaContextProvider id={id!}>
      <CompetenciaHeader />
      <Container text>
        <Routes>
          <Route path="/" element={<TablaPosiciones idCompetencia={id!} />} />
          <Route path="/equipos" element={<ListaEquipos onSelect={(e) => navigate(`/equipos/${e._id}`)} />} />
          <Route path="/fixture" element={<Fixture />} />
          <Route
            path="/estadisticas"
            element={
              <Segment placeholder>
                <Header icon>
                  <Icon name="settings" />
                  Aun no hay estadisticas.
                </Header>
              </Segment>
            }
          ></Route>
        </Routes>
      </Container>
    </CompetenciaContextProvider>
  )
}

const CompetenciaHeader = () => {

  const { competencia: { _id, nombre } } = useContext(CompetenciaContext);

  const navigate = useNavigate();

  const handleClick = (chip: IChipElement) => navigate(`/competencias/${_id}/${chip.url}`);

  return <Menu icon attached="bottom" inverted color='blue'>
    <Container>
      {routes.map(r => <Menu.Item name={r.icon} icon={r.icon} link onClick={() => handleClick(r)}>{ r.text }</Menu.Item>)}
    </Container>
  </Menu>
}

export default CompetenciaDetalle
