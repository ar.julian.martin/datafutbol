import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Header, Icon, Segment, Table } from 'semantic-ui-react'
import { IPosicion } from '../../types/posicion'
import { Equipo } from '../Equipo'

export const TablaPosiciones: React.FC<{ idCompetencia: string }> = ({ idCompetencia }) => {
  const [tabla, setTabla] = useState<IPosicion[]>([])

  useEffect(() => {
    axios.get(`/api/v1/competencias/${idCompetencia}/tabla`).then((resp) => setTabla(resp.data))
  }, [idCompetencia])

  return tabla.length ? (
    <Table striped unstackable compact="very">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell></Table.HeaderCell>
          <Table.HeaderCell>Pts</Table.HeaderCell>
          <Table.HeaderCell>PJ</Table.HeaderCell>
          <Table.HeaderCell>PG</Table.HeaderCell>
          <Table.HeaderCell>PE</Table.HeaderCell>
          <Table.HeaderCell>PP</Table.HeaderCell>
          <Table.HeaderCell>GF</Table.HeaderCell>
          <Table.HeaderCell>GC</Table.HeaderCell>
          <Table.HeaderCell>DG</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {tabla.map((posicion) => (
          <Table.Row key={posicion.equipo._id}>
            <Table.Cell singleLine>{<Equipo as="h5" {...posicion.equipo} />}</Table.Cell>
            <Table.Cell>{posicion.puntos}</Table.Cell>
            <Table.Cell>{posicion.partidosJugados}</Table.Cell>
            <Table.Cell>{posicion.partidosGanados}</Table.Cell>
            <Table.Cell>{posicion.partidosEmpatados}</Table.Cell>
            <Table.Cell>{posicion.partidosPerdidos}</Table.Cell>
            <Table.Cell>{posicion.golesFavor}</Table.Cell>
            <Table.Cell>{posicion.golesContra}</Table.Cell>
            <Table.Cell>{posicion.diferenciaGoles}</Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  ) : (
    <Segment placeholder>
      <Header icon>
        <Icon name="settings" />
        Aun no hay posiciones.
      </Header>
    </Segment>
  )
}
