import { ICompetencia } from './competencia'
import { IEquipo } from './equipos'
import { IEvent } from './event'

export interface IPartido {
  _id: string
  name: string
  competencia: ICompetencia['_id']
  equipoLocal: IEquipo
  equipoVisitante: IEquipo
  fecha: number
  resultado: { golesLocal: number; golesVisitante: number }
  eventos: IEvent[];
  updatedAt: Date
  createdAt: Date
}
