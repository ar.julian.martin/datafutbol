import React, { useRef } from 'react'
import { useNavigate } from 'react-router-dom'
import { List, Image, Input, Button, Header } from 'semantic-ui-react'
import { useTeams } from '@shared/hooks'
import trofeo from "@shared/assets/trofeo.png"

const Competencias: React.FC = () => {
  const navigate = useNavigate()
  const { teams, createTeam } = useTeams()

  const inputRef = useRef<HTMLInputElement>(null)

  const handleClick = async () => {
    const nombre = inputRef.current?.value
    if (!nombre) return
    createTeam(nombre)
    inputRef.current.value = ''
  }

  return (
    <div>
      <Header as="h3" block>
        Equipos
      </Header>
      {teams.length > 0 ? (
        <List divided relaxed selection>
          <Input placeholder="Crear competencia" action fluid>
            <input ref={inputRef} />
            <Button onClick={handleClick}>Crear</Button>
          </Input>
          {teams.map((team) => (
            <List.Item key={team._id} onClick={() => navigate(`/equipos/${team._id}`)}>
              <Image avatar src={trofeo} />
              <List.Content>
                <List.Header as="a">{team.nombre}</List.Header>
                <List.Description as="a">Updated 10 mins ago</List.Description>
              </List.Content>
            </List.Item>
          ))}
        </List>
      ) : (
        <Input placeholder="Crear equipo" action fluid>
          <input ref={inputRef} />
          <Button onClick={handleClick}>Crear</Button>
        </Input>
      )}
    </div>
  )
}

export default Competencias
