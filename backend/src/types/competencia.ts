import { Document } from 'mongoose'
import { IEquipo } from './equipo'
import { IPartido } from './partido'

export interface ICompetencia extends Document {
	nombre: string
	equipos: Array<IEquipo['_id']>
	partidos: Array<IPartido['_id']>
}
