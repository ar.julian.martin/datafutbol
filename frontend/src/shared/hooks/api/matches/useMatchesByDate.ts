import { DateTime } from "luxon";
import { useMemo } from "react";
import { useQuery } from "@shared/hooks/api/useQuery";
import { IPartido } from "@shared/types";

/**
 * Retrieve all matches for a specific date
 */
export const useMatchesByDate = (date: DateTime) => {

    const dateStr = useMemo(() => date.toFormat("yyyyMMdd"), [date]);
  
    return useQuery<IPartido[]>(`partidos/fecha/${dateStr}`, "get");
}