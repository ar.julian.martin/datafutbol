import React, { useEffect, useMemo, useState } from 'react'
import { IEquipo } from '../types/equipos'
import axios from 'axios'
import { Dropdown } from 'semantic-ui-react'

export const BuscarEquipo: React.FC<{ onSelect: (equipo: IEquipo) => void }> = ({ onSelect }) => {
  const [input, setInput] = useState('')
  const [isFetching, setIsFetching] = useState(false)
  const [options, setOptions] = useState<IEquipo[]>([])

  const dropdownOptions = useMemo(() => options.map((e: IEquipo) => ({ text: e.nombre, value: e._id })), [options])

  const handleSelect = (value: string) => {
    const equipo = options.find((e) => e._id === value)
    if (!equipo) return
    onSelect(equipo)
    setInput('')
    setOptions([])
  }

  useEffect(() => {
    const search = async () => {
      if (input.length < 3) {
        setOptions([])
        return
      }
      setIsFetching(true)
      const resp = await axios.get(`/api/v1/equipos?nombre=${input}`)
      setOptions(resp.data)
      setIsFetching(false)
    }

    const to = setTimeout(search, 500)
    return () => clearTimeout(to)
  }, [input])

  return (
    <Dropdown
      fluid
      selection
      search={true}
      options={dropdownOptions}
      value={input}
      placeholder="Agregar equipo"
      onChange={(_, { value }) => handleSelect(value as string)}
      onSearchChange={(_, { searchQuery }) => setInput(searchQuery)}
      disabled={isFetching}
      loading={isFetching}
    />
  )
  /*
    <>
      <Input fluid transparent icon="search" placeholder="Agregar equipo..." onChange={(e) => setInput(e.target.value)} value={input}></Input>
      {options.length > 0 && (
        <>
          <Divider />
          <ListaEquipos equipos={options} onSelect={onSelect} />
          <p>Seleccione un equipo para agregar a la competencia</p>
          <Divider />
        </>
      )}
    </>
  */
}
