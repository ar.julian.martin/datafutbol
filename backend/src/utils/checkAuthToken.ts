import { RequestHandler } from "express";
import admin, { ServiceAccount } from 'firebase-admin';
import serviceAccount from '../../config/serviceAccountKey.json';

// Inicializa Firebase Admin SDK
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount as ServiceAccount),
});

export const checkAuthToken: RequestHandler = async (req, res, next) => {
	const token = req.headers.authorization;
	if(!token)
		return res.status(401).json({ message: "Not authorized" });

	try {
		await admin.auth().verifyIdToken(token);
		next();
	} catch (error) {
		return res.status(401).json({ message: "Not authorized", error });
	}
}