import axios from 'axios'
import { useEffect, useState } from 'react'
import { IEquipo } from '@shared/types'

export const useTeams = () => {
  const [teams, setTeams] = useState<IEquipo[]>([])

  useEffect(() => {
    axios.get('/api/v1/equipos').then((resp) => setTeams(resp.data))
  }, [])

  const createTeam = (nombre: string) => {
    axios.post('/api/v1/equipos', { nombre }).then((resp) => setTeams((c) => [...c, resp.data]))
  }

  return { teams, createTeam }
}
