import { Document } from "mongoose";
import { ICompetencia } from "./competencia";
import { IEquipo } from "./equipo";
import { IEvento } from "./evento";

export interface IPartido extends Document {
  competencia: ICompetencia["_id"];
  fecha: number;
  dia: Date;
  equipoLocal: IEquipo["_id"];
  equipoVisitante: IEquipo["_id"];
  eventos: Array<IEvento['_id']>;
  resultado: {
    golesLocal: number;
    golesVisitante: number;
  };
}
