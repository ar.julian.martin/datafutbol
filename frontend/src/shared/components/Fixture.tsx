import React, { useContext } from 'react'
import { Button, Grid, Header, Icon, Segment } from 'semantic-ui-react'
import { CompetenciaContext } from '@shared/contexts/Competencia'
import { useNavigate } from 'react-router-dom'
import { Equipo } from './Equipo'

export const Fixture: React.FC = () => {

  const navigate = useNavigate();
  const { competencia: { equipos, partidos }, generarFixture } = useContext(CompetenciaContext);

  return partidos.length !== 0 ? <Segment>
    <Grid columns={3} textAlign="center">
      {partidos.map((partido) => <Grid.Row key={partido._id} onClick={() => navigate(`/partidos/${partido._id}`)}>
        <Grid.Column textAlign="left">
          <Equipo {...partido.equipoLocal!} />
        </Grid.Column>
        <Grid.Column verticalAlign="middle">{partido.resultado ? `${partido.resultado.golesLocal} - ${partido.resultado.golesVisitante}` : 'vs'}</Grid.Column>
        <Grid.Column textAlign="right">
          <Equipo espejado {...partido.equipoVisitante!} />
        </Grid.Column>
      </Grid.Row>)}
    </Grid>
  </Segment> : <Segment placeholder>
    <Header icon>
      <Icon name="settings" />
      Aun no hay partidos.
    </Header>
    <Segment.Inline>
      <Button primary disabled={equipos.length < 3} onClick={generarFixture}>
        Generar fixture
      </Button>
      <Button color="red">Eliminar competencia</Button>
    </Segment.Inline>
  </Segment>
}
