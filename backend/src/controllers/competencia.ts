import e, { Request, Response } from 'express'
import { Competencia } from '../models/competencia'
import { Equipo } from '../models/equipo'
import { Partido } from '../models/partido'
import { IEquipo } from '../types/equipo'
import { IPartido } from '../types/partido'
import { CalcularTablaPosiciones } from '../utils/calcularTablaPosiciones'

export const crearCompetencia = async (req: Request, res: Response) => {
	try {
		const competencia = new Competencia(req.body)
		await competencia.save()
		res.status(201).json(competencia)
	} catch (error: any) {
		res.status(500).json({ error: error.message })
	}
}

export const obtenerCompetencias = async (req: Request, res: Response) => {
	try {
		const competencias = await Competencia.find().populate('equipos')
		res.status(200).json(competencias)
	} catch (error: any) {
		res.status(500).json({ error: error.message })
	}
}

// Controlador para obtener una competencia por su ID
export const obtenerCompetenciaPorId = async (req: Request, res: Response) => {
	try {
		const competencia = await Competencia.findById(req.params.id).populate('equipos').populate({ path: 'partidos', populate: ['equipoLocal', 'equipoVisitante'] })
		if (!competencia) return res.status(404).json({ mensaje: 'No se encontró la competencia' })
		res.status(200).json(competencia)
	} catch (error: any) {
		res.status(500).json({ mensaje: 'Error al obtener la competencia', error })
	}
}

// Controlador para agregar un equipo a una competencia
export const agregarEquipoACompetencia = async (req: Request, res: Response) => {
	try {
		const equipo = await Equipo.findById(req.body.equipoId)
		if (!equipo) 
			return res.status(404).json({ mensaje: 'Equipo no encontrado' });
		
		const competencia = await Competencia.findById(req.params.id).populate('equipos')
		if (!competencia) 
			return res.status(404).json({ mensaje: 'Competencia no encontrada' });
		
		if(competencia.equipos.includes(equipo)) 
			return res.status(500).json({ mensaje: 'El equipo ya esta incluido en la competencia.' })

		competencia.equipos.push(equipo)
		await competencia.save()
		res.status(201).json(competencia)
	} catch (error: any) {
		res.status(500).json({ mensaje: 'Error al agregar el equipo a la competencia', error })
	}
}

// Controlador para eliminar un equipo de una competencia
export const eliminarEquipoDeCompetencia = async (req: Request, res: Response) => {
	try {
		const equipo = await Equipo.findById(req.params.equipoId);
		if (!equipo) 
			return res.status(404).json({ mensaje: 'Equipo no encontrado' });

		const competencia = await Competencia.findById(req.params.id);
		if (!competencia) 
			return res.status(404).json({ mensaje: 'Competencia no encontrada' });

		const equipoIndex = competencia.equipos.indexOf(equipo._id);
		if (equipoIndex === -1) 
			return res.status(404).json({ mensaje: 'El equipo no se encuentra en la competencia' });

		competencia.equipos.splice(equipoIndex, 1);
		await competencia.save();

		res.status(201).json(competencia)
	} catch (error: any) {
		res.status(500).json({ mensaje: 'Error al agregar el equipo a la competencia', error })
	}
}

// Controlador para generar los partidos de una competencia
export const generarPartidos = async (req: Request, res: Response) => {
	try {
		const competencia = await Competencia.findById(req.params.id).populate('equipos')
		if (competencia) {
			const equipos = competencia.equipos as [IEquipo]
			const partidos: IPartido[] = []

			for (let i = 0; i < equipos.length; i++) {
				for (let j = i + 1; j < equipos.length; j++) {
					const partido = new Partido({
						competencia: competencia._id,
						equipoLocal: equipos[i]._id,
						equipoVisitante: equipos[j]._id,
						fecha: i + 1,
					})
					partidos.push(partido)
				}
			}

			await Partido.insertMany(partidos)
			competencia.partidos = partidos
			await competencia.save()
			res.status(201).json(partidos)
		} else {
			res.status(404).json({ mensaje: 'No se encontró la competencia' })
		}
	} catch (error: any) {
		res.status(500).json({ mensaje: 'Error al generar los partidos', error })
	}
}

// Controlador para obtener la tabla de posiciones de una competencia
export const obtenerTablaPosiciones = async (req: Request, res: Response) => {
	try {
		const competencia = await Competencia.findById(req.params.id).populate('equipos')
		if (competencia) {
			const equipos = competencia.equipos as [IEquipo]
			const partidos = await Partido.find({ competencia: competencia._id })

			const tablaPosiciones = CalcularTablaPosiciones(equipos, partidos)

			res.status(200).json(tablaPosiciones)
		} else {
			res.status(404).json({ mensaje: 'No se encontro la competencia' })
		}
	} catch (error: any) {
		res.status(500).json({ mensaje: 'Error al obtener la tabla', error })
	}
}
