import axios, { AxiosError } from "axios";
import { useEffect, useState } from "react"

export const useQuery = <T, P = any>(endpoint: string, method: "get" | "post" | "put" | "delete", params?: P) => {

    const [data, setData] = useState<T>()
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState<AxiosError>();

    useEffect(() => {
        setLoading(true);
        const token = localStorage.getItem("token");
        axios.request({ url: `/api/v1/${endpoint}`, method, data: params, headers: { Authorization: token }  })
             .then(resp => setData(resp.data))
             .catch(setError)
             .finally(() => setLoading(false))
    }, [endpoint, params, method])

    return {
        loading,
        error,
        data
    }
}